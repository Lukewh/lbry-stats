import subprocess
import json
from math import fabs
from os import path

from download_sdk import download_sdk

WARNING = "\033[31m"
RESET = "\033[0m"
OK = "\033[32m"
BOLD = "\033[1m"

if not path.exists("lbrynet"):
    print("SDK doesn't exist - downloading")
    download_sdk()

def get_transation_page(page, items):
    print("Get page " + str(page))
    output = subprocess.check_output("./lbrynet transaction list --page=" + str(page) + " --page_size=500", shell=True)
    result = json.loads(output)

    items = items + result.get("items", [])

    if page < result.get("total_pages", page):
        return get_transaction_page(page + 1, items)

    return items

def get_stream_page(page, items):
    print("get page " + str(page))
    output = subprocess.check_output("./lbrynet stream list --page=" + str(page) + " --page_size=500", shell=True)
    result = json.loads(output)

    for item in result["items"]:
        items[item["claim_id"]] = item

    if page < result.get("total_pages", page):
        return get_stream_page(page + 1, items)

    return items

print("Getting streams")

streams = get_stream_page(1, {})

print("Done")

print("Getting transactions")

transactions = get_transation_page(1, [])

def get_info(items, key):
    result = {}
    for item in items:
        if key in item and item[key]:
            for info in item[key]:
                name = info["claim_name"]
                if name not in result:
                    result[name] = []

                result[name].append(info)

    return result

raw_transactions = [get_info(transactions, "update_info"), get_info(transactions, "claim_info"), get_info(transactions, "support_info")]

transactions = {}

for info in raw_transactions:
    for claim_key in info:
        if claim_key not in transactions:
            transactions[claim_key] = []

        for item in info[claim_key]:
            transactions[claim_key].append(item)

all_income = 0.0
all_spend = 0.0
all_income_count = 0
all_spend_count = 0

for claim_key in transactions:
    claim_data = transactions[claim_key]

    if claim_data[0]["claim_id"] in streams:
        claim_info = streams[claim_data[0]["claim_id"]]

        print(BOLD + claim_info["value"]["title"] + RESET)
    else:
        print(BOLD + claim_key + RESET)

    tips = 0.0
    total_tips = 0
    spend = 0.0
    total_spend = 0

    for item in claim_data:
        if float(item["balance_delta"]) > 0.0:
            tips = tips + float(item["balance_delta"])
            total_tips = total_tips + 1
        else:
            spend = spend + fabs(float(item["balance_delta"]))
            total_spend = total_spend + 1

    if tips > 0.0:
        avg_tips = tips / float(total_tips)
    else:
        avg_tips = 0.0

    if spend > 0.0:
        avg_spend = spend / float(total_spend)
    else:
        avg_spend = 0.0

    all_income = all_income + tips
    all_spend = all_spend + spend

    all_income_count = all_income_count + total_tips
    all_spend_count = all_spend_count + total_spend

    print("\tReceived:\t" + BOLD + str(tips) + "LBC" + RESET + " from " + BOLD + str(total_tips) + RESET + " tips\t" + BOLD + str(avg_tips) + RESET + " average")
    print("\tSpent:\t\t" + BOLD + str(spend) + "LBC" + RESET + " in " + BOLD + str(total_spend) + RESET + " spends\t" + BOLD + str(avg_spend) + RESET + " average")
    profit = tips - spend
    if profit > 0.0:
        color = OK
    else:
        color = WARNING
    print("\t" + BOLD + color + str(profit) + "LBC" + RESET)
    print("")

all_avg_income = all_income / float(all_income_count)
all_avg_spend = all_spend / float(all_spend_count)

print("Received:\t" + BOLD + str(all_income) + "LBC" + RESET + " from " + BOLD + str(all_income_count) + RESET + " tips\t" + BOLD + str(all_avg_income) + RESET + " average")
print("Spend:\t\t" + BOLD + str(all_spend) + "LBC" + RESET + " in " + BOLD + str(all_spend_count) + RESET + " spend\t" + BOLD + str(all_avg_spend) + RESET + " average")

profit_loss = all_income - all_spend

if profit_loss > 0.0:
    color = OK
else:
    color = WARNING

print("")
print(BOLD + "PROFIT/LOSS:\t" + color + str(all_income - all_spend) + "LBC" + RESET)

print("")

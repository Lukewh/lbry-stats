import requests
from zipfile import ZipFile
from urllib.request import urlretrieve
import os
from stat import *

def download_sdk():
    try:
         response = requests.get("https://api.github.com/repos/lbryio/lbry-sdk/releases/latest")

         VERSION = response.json().get("tag_name")
    except Exception as error:
         print(error)

    print("Getting version " + VERSION)

    urlretrieve(
         "https://github.com/lbryio/lbry-sdk/releases/download/" + VERSION + "/lbrynet-linux.zip",
         "lbrynet-linux.zip"
    )

    print("Done")

    print("Extracting")

    with ZipFile("lbrynet-linux.zip", "r") as zip:
         zip.extractall(".")

    print("Done")

    print("Making executable")

    st = os.stat("lbrynet")
    os.chmod("lbrynet", st.st_mode | S_IEXEC)

    print("All done")

if __name__ == "__main__":
     download_sdk()
